class Account{
	String bank_acc_name;
	Long acc_number;
	long mobile_number;
	Double balance;

	public Account(String bank_acc_name,Long acc_number,Long mobile_number,Double balance){
		this.bank_acc_name = bank_acc_name;
		this.acc_number = acc_number;
		this.mobile_number = mobile_number;
		this.balance = balance;
	}
	public void deposit(double bal){

		balance = balance + bal;
	}
	public void withdrwal(double bal){
		if(balance>=bal){
			balance = balance - bal;
		}
		else{
			System.out.println("Insufficient Balance");
		}
		
	}
	public void display(){
		System.out.println("Bank Account Number 	: " +bank_acc_name);
		System.out.println("Bank Account Name 		: " +acc_number);
		System.out.println("Bank Account Mobile 	: " +mobile_number);
		System.out.println("Bank Account Balance 	: " +balance);
	}
	public static void main(String[] args) {
		Account a1 = new Account("Atif Hasan",123456789l,1234567890l,100.00);
		a1.display();
		System.out.println("--------------------------------------------------");
		a1.deposit(1000.00);
		a1.display();

		System.out.println("_______________________________________________________");

		Account a2 = new Account("Danish Hasan",123456789l,123456789l,200.00);
		a2.display();
		System.out.println("--------------------------------------------------");
		a2.withdrwal(100);
		a1.display();
		System.out.println("------------------------------------------------------");
		a1.withdrwal(4.00);
		a1.display();
		System.out.println("--------------------------------------------------------");
		a2.withdrwal(33.00);
		a2.display();
	}
}
